source ~/stackrc
cd ~

openstack overcloud deploy --templates \
-e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /home/stack/templates/network-environment.yaml \
-e /home/stack/templates/neutron-nuage-config.yaml \
-e /home/stack/templates/nova-nuage-config.yaml \
--control-flavor control \
--compute-flavor compute \
--control-scale 1 \
--compute-scale 1 \
--ceph-storage-scale 0 \
--ntp-server time.nist.gov \
--neutron-tunnel-types vxlan \
--neutron-network-type vxlan 
